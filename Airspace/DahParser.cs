﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Geo;
using System.Diagnostics;

namespace Airspace
{
    class DahParser : Parser
    {
        public override string FormatName { get { return "DAH"; } }

        public override Airspaces Parse(string name, string source)
        {
            //TODO: Check for self-overlap: YBBB/DARWIN CONTROL ZONE (C)

            var airspaces = new Airspaces
            {
                Name = name,
                Sections = new List<Section>()
            };

            DateTime.TryParse(Regex.Match(source, "^EFFECTIVE (.+)$", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups[1].Value, out airspaces.ValidDate);

            //Clean source
            //Remove page header and footer text
            source = Regex.Replace(source, @" *\r", "");
            source = Regex.Replace(source, @"[\r\n]*^DAH Effective Date .+$([\r\n]*\d\d?$)?", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            //Clean up dangling page headers
            source = Regex.Replace(source, @"( *(CTR C|ARF/AQZ|RWY THR|UN C/R ALA|NAV|IFR WPT|AIR ROUTES .|SENS|Section \d+)( -)? *\d* *)?  AIP AUSTRALIA.*$", "", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Match dangling = Regex.Match(source, @"^(?!.*DAH Effective Date ).*AIP AUSTRALIA  SECTION", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            if (dangling.Success)
                RaiseWarning("Dangling page header not cleaned up.", dangling.ToString());

            var sectionsSource = Regex.Split(source, @"(?=^SECTION [0-9]+ (?:- )?.+$)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            RaiseDeclareTaskSize(sectionsSource.Length);
            foreach (string sectionSource in sectionsSource)
            {
                string sectionName = Regex.Match(sectionSource, @"^SECTION [0-9]+ (?:[-–] )?(?<Name>.+)$", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups["Name"].Value;
                if (sectionName.ToUpper() == "PRD AREA")
                {
                    foreach (string subSectionSource in Regex.Split(sectionSource, @"^(?=PROHIBITED AREAS?$|RESTRICTED AREAS?$|DANGER AREAS?$)", RegexOptions.IgnoreCase | RegexOptions.Multiline))
                    {
                        string subSectionName = Regex.Match(subSectionSource, @"^(?<PRD>PROHIBITED AREA|RESTRICTED AREA|DANGER AREA)S?$", RegexOptions.IgnoreCase | RegexOptions.Multiline).Groups["PRD"].Value;
                        Section section = ParseSection(subSectionName, subSectionSource);
                        if (section.Airspaces.Count > 0)
                            airspaces.Sections.Add(section);
                    }
                }
                else
                {
                    Section section = ParseSection(sectionName, sectionSource);
                    if (section.Airspaces.Count > 0)
                        airspaces.Sections.Add(section);
                }
                RaiseTaskProgress();
                if (cancel)
                    break;
            }
            return airspaces;
        }

        private Section ParseSection(string name, string sectionSource)
        {
            Regex latLimits = new Regex(@"(?:A|then along the (?<arc>minor|major) arc of a)? circle of (?<radius>[\d\.]+)NM radius centred on (?<centre>\d\d \d\d \d\d[NS] \d\d\d \d\d \d\d[EW](?: \((?<centreName>.+?)\))?)|(?<point>\d\d \d\d \d\d[NS] \d\d\d \d\d \d\d[EW])|then along (?<feature>.+?) to");
            Regex regexWhitespace = new Regex(@"\s+");
            Regex airspaceRegex = new Regex(@"(?<Airspace>^ *(YMMM|YBBB|YMMM-YBBB|YBBB-YMMM)/.+$\n^[\S\s\n]+?^(?:Controlling Authority|Contact):.*)$",
                RegexOptions.IgnoreCase | RegexOptions.Multiline);

            Regex airspaceParserRegex = new Regex(
                @"^ *(?<FIR>YMMM|YBBB|YMMM-YBBB|YBBB-YMMM)/(?<Item>.+)(?:[\r\n]+(?<Mil>Joint Defence Facility)$)?(?:[\r\n]+Conditional Status: (?<CondStatus>.+)$)?(?:[\r\n]+(?<Hazard>.+)$)?[\r\n]+^LATERAL LIMITS: (?<Lateral>[\S\s\r\n]+?)(?:^Notes: (?<Notes>[\S\s\r\n]+?)$)?[\r\n]+Vertical Limits: (?<LowerLimit>.+?) - (?<UpperLimit>.+?)$(?:[\r\n]+Hours of activity: (?<Hours>[\S\s\r\n]+?)$)?[\r\n]+(?:Controlling Authority: *(?<ControllingAuth>.*(?:[\r\n]+.*?FLT$)?)|(?:Contact: (?<Contact>.*)))",
                RegexOptions.IgnoreCase | RegexOptions.Multiline);

            Section section = new Section(name);
            string sectionFriendlyName = null;

            Match airspaceMatch = airspaceRegex.Match(sectionSource);
            while (airspaceMatch.Success && !cancel)
            {
                if (sectionFriendlyName == null)
                    sectionFriendlyName = FriendlyName(section.Name);
                string airspaceString = airspaceMatch.Groups["Airspace"].Value;
                Match airspaceParserMatch = airspaceParserRegex.Match(airspaceString);

                if (airspaceParserMatch.Success)
                {
                    var airspace = new Airspace()
                    {
                        FIR = airspaceParserMatch.Groups["FIR"].Value,
                        Name = airspaceParserMatch.Groups["Item"].Value,
                        Type = sectionFriendlyName,
                        IsJointDefenseFacility = (airspaceParserMatch.Groups["Mil"].Value != ""),
                        ConditionalStatus = airspaceParserMatch.Groups["CondStatus"].Value,
                        Hazard = airspaceParserMatch.Groups["Hazard"].Value,
                        LateralLimitsDesc = Regex.Replace(airspaceParserMatch.Groups["Lateral"].Value, @"[\r\n]+", " "),
                        Notes = regexWhitespace.Replace(airspaceParserMatch.Groups["Notes"].Value, " "),
                        LowerLimit = airspaceParserMatch.Groups["LowerLimit"].Value,
                        UpperLimit = airspaceParserMatch.Groups["UpperLimit"].Value,
                        HoursOfActivity = airspaceParserMatch.Groups["Hours"].Value,
                        ControllingAuthority = airspaceParserMatch.Groups["ControllingAuth"].Value,
                        Contact = airspaceParserMatch.Groups["Contact"].Value
                    };
                    airspace.LateralLimitElements = ParseLateralLimits(latLimits.Match(airspace.LateralLimitsDesc));
                    section.Airspaces.Add(airspace);
                }
                else
                    RaiseError("Could not parse airspace definition.", airspaceMatch.Groups["Airspace"].Value);

                airspaceMatch = airspaceMatch.NextMatch();
            }

            return section;
        }

        private string FriendlyName(string airspaceType)
        {
            switch (airspaceType)
            {
                case "FLIGHT INFORMATION REGIONS":
                case "CONTROL ZONES C":
                case "CONTROL ZONES D":
                case "CONTROLLED AIRSPACE A":
                case "CONTROLLED AIRSPACE C":
                case "CONTROLLED AIRSPACE D":
                case "CONTROLLED AIRSPACE E":
                case "OCEANIC CONTROL AREAS":
                case "E AIRSPACE FREQUENCY BOUNDARIES":
                case "FLIGHT INFORMATION AREAS":
                    return ToProperCase(airspaceType);
                case "ATC SECTORS LOW":
                    return "ATC Sectors Low";
                case "ATC SECTORS HIGH/LOW":
                    return "ATC Sectors High/Low";
                case "PROHIBITED AREA":
                    return "Prohibited Area";
                case "RESTRICTED AREA":
                    return "Restricted Area";
                case "DANGER AREA":
                    return "Danger Area";
                default:
                    RaiseWarning("Unknown airspace type.", airspaceType);
                    return airspaceType;
            }
        }

        private List<ILateralLimitElement> ParseLateralLimits(Match source)
        {
            var elements = new List<ILateralLimitElement>();

            while (source.Success)
            {
                if (source.Groups["point"].Success)
                    elements.Add(new LateralLimitElementPoint()
                    {
                        Point = LatLngFromString(source.Groups["point"].Value)
                    });
                else if (source.Groups["arc"].Success)
                {
                    Match arcSource = source;
                    source = source.NextMatch();

                    LatLng centre = LatLngFromString(arcSource.Groups["centre"].Value);
                    LatLng start = ((LateralLimitElementPoint)elements[elements.Count - 1]).Point;
                    LatLng end = LatLngFromString(source.Groups["point"].Value);

                    double bearingStart = centre.BearingTo(start);
                    double bearingEnd = centre.BearingTo(end);
                    elements.Add(new LateralLimitElementArc()
                    {
                        Centre = centre,
                        Radius = Convert.ToDouble(arcSource.Groups["radius"].Value) * LatLng.MetersPerNauticalMile,
                        BearingStart = bearingStart,
                        BearingEnd = bearingEnd,
                        Clockwise = LatLng.GetBearingChangeByMajorMinor(bearingStart, bearingEnd, (arcSource.Groups["arc"].Value == "major")) >= 0
                    });
                    elements.Add(new LateralLimitElementPoint()
                    {
                        Point = end
                    });
                }
                else if (source.Groups["centre"].Success)
                {
                    elements.Add(new LateralLimitElementCircle()
                    {
                        Centre = LatLngFromString(source.Groups["centre"].Value),
                        Radius = Convert.ToDouble(source.Groups["radius"].Value) * LatLng.MetersPerNauticalMile
                    });
                }
                else if (source.Groups["feature"].Success)
                {
                    string desc = source.Groups["feature"].Value;

                    elements.Add(new LateralLimitElementAlongFeature()
                    {
                        FeatureDescription = desc,
                    });
                }
                else
                    RaiseWarning("Lateral boundary unrecognised.", source.Value);
                source = source.NextMatch();

            }

            if (elements.Count > 1)
            {
                var firstPt = ((LateralLimitElementPoint)elements[0]).Point;
                var LastPt = ((LateralLimitElementPoint)elements[elements.Count - 1]).Point;
                if (firstPt.lat == LastPt.lat && firstPt.lng == LastPt.lng)
                    elements.RemoveAt(elements.Count - 1); // Remove duplicate last point
            }
            return elements;
        }

        private static LatLng LatLngFromString(string source)
        {
            GroupCollection coords = Regex.Match(source, @"(\d\d) (\d\d) (\d\d)([NS]) (\d\d\d) (\d\d) (\d\d)([EW])").Groups;
            return new LatLng(coords[4].Value == "N" ? HemisphereNS.N : HemisphereNS.S, Convert.ToUInt16(coords[1].Value), Convert.ToDouble(coords[2].Value), Convert.ToDouble(coords[3].Value),
                coords[8].Value == "E" ? HemisphereEW.E : HemisphereEW.W, Convert.ToUInt16(coords[5].Value), Convert.ToDouble(coords[6].Value), Convert.ToDouble(coords[7].Value));
        }

        private static string ToProperCase(string text)
        {
            return System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(text.ToLower(System.Globalization.CultureInfo.InvariantCulture));
        }

    }
}
