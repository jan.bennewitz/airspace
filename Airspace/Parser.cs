﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airspace
{
    abstract class Parser : Converter
    {
        abstract public Airspaces Parse(string name, string source);
    }
}