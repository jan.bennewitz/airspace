﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geo;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace Airspace
{
    class OpenAirWriter : Converter
    {
        public override string FormatName
        { get { return "OpenAir"; } }

        public String WriteToOpenAir(Airspaces airspaceData)
        {
            var result = new StringBuilder();

            result.AppendLine(GetHeader(airspaceData));

            RaiseDeclareTaskSize(airspaceData.Sections.Count);
            foreach (Section section in airspaceData.Sections)
            {
                switch (section.Name.ToUpper())
                {
                    case "CONTROL ZONES C":
                    case "CONTROL ZONES D":
                    case "CONTROLLED AIRSPACE A":
                    case "CONTROLLED AIRSPACE C":
                    case "CONTROLLED AIRSPACE D":
                    case "CONTROLLED AIRSPACE E":
                    case "RESTRICTED AREA":
                    case "PROHIBITED AREA":
                    case "DANGER AREA":
                    case "A":
                    case "C":
                    case "D":
                    case "E":
                    case "CTR":
                    case "Q":
                    case "R":
                    case "G":
                    case "RMZ":

                        foreach (var airspace in section.Airspaces)
                            result.Append(AirspaceDescription(airspace));
                        break;

                    case "FLIGHT INFORMATION REGIONS":
                    case "OCEANIC CONTROL AREAS":
                    case "ATC SECTORS LOW":
                    case "ATC SECTORS HIGH/LOW":
                    case "E AIRSPACE FREQUENCY BOUNDARIES":
                    case "FLIGHT INFORMATION AREAS":
                        // Discard
                        break;

                    default:
                        RaiseError("Section has unrecognised airspace type", section.Name);
                        break;
                }
                RaiseTaskProgress();
            }

            return result.ToString();
        }

        private static string GetHeader(Airspaces airspaceData)
        {
            string validDate = airspaceData.ValidDate.ToString("yyyy MMMM d");

            return @"********************************************************************************
*                                                                              *
*                               Australian Airspace                            *
*                                                                              *
*                            Dated " + validDate + new string(' ', 44 - validDate.Length) + @"*
*                                                                              *
*           >>>>>>>>>> UNOFFICIAL, USE AT YOUR OWN RISK <<<<<<<<<<             *
*                                                                              *
*         Do not use for navigation, for flight verification only              *
*                                                                              *
*                  Always consult the relevant publications                    *
*                  for current and correct information. This                   *
*                   service is provided free of charge with                    *
*                    no warrantees, expressed or implied.                      *
*                        User assumes all risk of use.                         *
*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^";
        }

        private string AirspaceDescription(Airspace airspace)
        {
            if (airspace.Type == "G" && airspace.Name.EndsWith(" UNCR"))
                return ""; // Skip uncertified airfields

            var result = new StringBuilder();

            // Emulate John W's file:
            //string id, name;
            //GetNameAndId(airspace, out id, out name);

            //result.AppendLine("* >> NAME " + name + "; ID: " + id + " <<");
            //result.AppendLine("AC " + GetAirspaceClass(airspace));
            //result.AppendLine("AN " + id);
            //result.AppendLine("AH " + airspace.UpperLimit);
            //result.AppendLine("AL " + airspace.LowerLimit);


            result.AppendLine("AC " + GetAirspaceClass(airspace));
            result.AppendLine("AN " + airspace.Name);
            result.AppendLine("AH " + airspace.UpperLimit);
            result.AppendLine("AL " + airspace.LowerLimit);

            //Extended:
            AppendExtended(result, airspace);

            Boolean currentTurnDirIsClockwise = true;
            foreach (var element in airspace.LateralLimitElements)
                AppendElementDescription(result, element, ref currentTurnDirIsClockwise);

            return result.ToString();
        }

        private static void AppendExtended(StringBuilder output, Airspace airspace) // Custom extension to OpenAir format
        {
            AppendExtendedField(output, "FIR", airspace.FIR);
            AppendExtendedField(output, "Type", airspace.Type);
            if (airspace.IsJointDefenseFacility)
                AppendExtendedField(output, "Joint Defense Facility");
            AppendExtendedField(output, "Conditional Status", airspace.ConditionalStatus);
            AppendExtendedField(output, "Hazard", airspace.Hazard);
            AppendExtendedField(output, "Notes", airspace.Notes);
            AppendExtendedField(output, "Hours of Activity", airspace.HoursOfActivity);
            AppendExtendedField(output, "Controlling Authority", airspace.ControllingAuthority);
            AppendExtendedField(output, "Contact", airspace.Contact);
        }
        private static void AppendExtendedField(StringBuilder output, string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                AppendExtendedField(output, name);
                foreach (string line in Regex.Split(value, @"\r\n|\r|\n"))
                    output.AppendLine("AD " + line);
            }
        }
        private static void AppendExtendedField(StringBuilder output, string name)
        {
            output.AppendLine("AD :" + name);
        }

        private static void GetNameAndId(Airspace airspace, out string id, out string name)
        {
            // This procedure is written to give the same result as the existing OpenAir file with no understanding of the function of separation between id and name.
            string regex;
            switch (airspace.Type)
            {
                case "Restricted Area":
                case "Prohibited Area":
                case "Danger Area":
                    regex = @"^(?<id>\w+) (?<name>.+)$";
                    break;
                case "Flight Information Areas":
                case "E Airspace Frequency Boundaries":
                    regex = @"^(?<id>.+) (?<name>\d+(\.\d+)([A-Z]\d?|A Z)?)$";
                    break;

                default:
                    regex = @"^(?<id>.+?)(?<name>(CTA .+|(((TMA|ARRIVALS) ?)?\b[A-Z])|CONTROL ZONE \([A-Z]\)))?$";
                    break;
            }

            var groups = Regex.Match(airspace.Name, regex, RegexOptions.Multiline).Groups;
            id = groups["id"].Value;
            name = groups["name"].Value;
        }

        private string GetAirspaceClass(Airspace airspace)
        {
            switch (airspace.Type)
            {
                case "Control Zones C":
                case "C":
                    return "C";
                case "Control Zones D":
                case "D":
                    return "D";
                case "Controlled Airspace A":
                case "A":
                    return "A";
                case "Controlled Airspace C":
                case "CTR":
                    return "C";
                case "Controlled Airspace D":
                    return "D";
                case "Controlled Airspace E":
                case "E":
                    return "E";
                case "Restricted Area":
                case "R":
                    return "R";
                case "Prohibited Area":
                case "P":
                    return "P";
                case "Danger Area":
                case "Q":
                    return "Q";

                case "G":
                    return "G";

                default:
                    RaiseError("Airspace type " + airspace.Type + " not recognised", airspace.Name);
                    return null;
            }
        }

        private void AppendElementDescription(StringBuilder output, ILateralLimitElement element, ref Boolean currentTurnDirIsClockwise)
        {
            if (element is LateralLimitElementPoint)
                output.AppendLine("DP " + GetPointString(((LateralLimitElementPoint)element).Point));
            else if (element is LateralLimitElementArc)
            {
                var arc = (LateralLimitElementArc)element;
                double bearingChange = LatLng.GetBearingChangeByDirection(arc.BearingStart, arc.BearingEnd, arc.Clockwise);

                //double distStart = start.DistanceTo(arcPoints[0]);
                //double distEnd = end.DistanceTo(arcPoints[arcPoints.Count - 1]);
                //Debug.Assert(double.IsNaN(distStart) || distStart / arc.Radius < .025, "Arc does not match start point. Distance: " + distStart + ", radius: " + arc.Radius + ", ratio: " + distStart / radius);
                //Debug.Assert(double.IsNaN(distEnd) || distEnd / arc.Radius < .025, "Arc does not match end point. Distance: " + distEnd + ", radius: " + arc.Radius + ", ratio: " + distEnd / radius);
                if ((bearingChange >= 0) != currentTurnDirIsClockwise)
                {
                    currentTurnDirIsClockwise = !currentTurnDirIsClockwise;
                    output.AppendLine("V D=" + (currentTurnDirIsClockwise ? "+" : "-"));
                }
                output.AppendLine("V X=" + GetPointString(arc.Centre));
                output.AppendLine("DA " + arc.Radius / LatLng.MetersPerNauticalMile + ", " +
                    Math.Round(LatLng.RadToDeg(arc.BearingStart), 3) + ", " +
                    Math.Round(LatLng.RadToDeg(arc.BearingEnd), 3));
            }
            else if (element is LateralLimitElementCircle)
            {
                var circle = (LateralLimitElementCircle)element;
                output.AppendLine("V X=" + GetPointString(circle.Centre));
                output.AppendLine("DC " + circle.Radius / LatLng.MetersPerNauticalMile);
            }
            else if (element is LateralLimitElementAlongFeature)
                output.AppendLine("DF " + ((LateralLimitElementAlongFeature)element).FeatureDescription); // "Follow feature", custom extension to OpenAir format.
            else
                RaiseError("Element not recognised", element.ToString());

            return;
        }

        private static string GetPointString(LatLng point)
        {
            return (GetCoordinateString(point.lat, "N", "S") + " " + GetCoordinateString(point.lng, "E", "W"));
        }

        private static string GetCoordinateString(double coordinate, string positiveHemisphere, string negativeHemisphere)
        {
            double degAbs = Math.Abs(coordinate);

            int totalSec = (int)Math.Round(degAbs * 60 * 60);
            int sec = totalSec % 60;
            int totalMin = (totalSec - sec) / 60;
            int min = totalMin % 60;
            int deg = (totalMin - min) / 60;

            //int wholeDeg = (int)Math.Floor(degAbs);
            //double minutes = (degAbs % 1) * 60;
            //int min = (int)Math.Floor(minutes);
            //double seconds = (minutes % 1) * 60;
            //int sec = (int)Math.Round(seconds);

            string hemisphere = coordinate >= 0 ? positiveHemisphere : negativeHemisphere;

            return deg + ":" + min.ToString("D2") + ":" + sec.ToString("D2") + " " + hemisphere;
        }
    }
}
