﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Kml;
using System.Threading.Tasks;

namespace Airspace
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void TestButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            var task = Task.Factory.StartNew(() => Test(sourceTextBox.Text));
            task.ContinueWith(t => EnableUI());
        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
            EnableUI(false);
            var task = Task.Factory.StartNew(() =>
            {
                try
                {
                    string source = sourceTextBox.Text;

                    //Parse as DAH
                    Airspaces airspaceData;
                    Airspaces fromDah = ParseFile(new DahParser(), source);
                    Airspaces fromOpenAir = ParseFile(new OpenAirParser(), source);
                    if (fromDah.GetAirspacesCount() + fromOpenAir.GetAirspacesCount() == 0)
                        report("No airspace data found.");
                    if (fromDah.GetAirspacesCount() > fromOpenAir.GetAirspacesCount())
                    {
                        airspaceData = fromDah;
                        report("Identified as DAH format.");
                    }
                    else
                    {
                        airspaceData = fromOpenAir;
                        report("Identified as OpenAir format.");
                    }
                    report(airspaceData.GetAirspacesCount() + " airspaces read.");

                    airspaceData.RemoveRepeatedLateralLimitsElements();

                    airspaceData = Save(airspaceData, sourceTextBox.Text);
                }
                catch (Exception ex)
                {
                    report("Exception: " + ex.Message);
                }
                    
            });
            task.ContinueWith(t => EnableUI());
        }

        private Airspaces Save(Airspaces airspaceData, string baseFileName)
        {
            report("Generating Kml ...");
            var kmlWriter = new KmlWriter();
            KmlDocument kml = kmlWriter.WriteToKml(airspaceData);
            kml.Save(targetTextBox.Text + ".kml");
            kml.SaveAsKmz(baseFileName + ".kmz");
            report("Saved Kml and Kmz.");

            report("Generating OpenAir ...");
            var openAirWriter = new OpenAirWriter();
            using (var outfile = new StreamWriter(baseFileName + ".OpenAir.txt"))
                outfile.Write(openAirWriter.WriteToOpenAir(airspaceData));
            report("Saved OpenAir.");
            return airspaceData;
        }

        private void EnableUI(Boolean enable = true)
        {
            this.InvokeIfReq(f =>
            {
                f.sourceTextBox.Enabled = enable;
                f.targetTextBox.Enabled = enable;
                f.ConvertButton.Enabled = enable;
                f.TestButton.Enabled = enable;
                f.progress.Value = 0;
            });
        }

        private void Test(string source)
        {
            Airspaces airspaceData;

            //Parse from DAH
            airspaceData = ParseFile(new DahParser(), source);

            //Generate Kml
            report("Generating Kml and Kmz ...");
            var kmlWriter = new KmlWriter();
            KmlDocument kml = kmlWriter.WriteToKml(airspaceData);
            kml.Save(source+ ".kml");
            kml.SaveAsKmz(source + ".kmz");
            report("Generated Kml and Kmz.");

            //Generate OpenAir
            report("Generating OpenAir ...");
            var openAirWriter = new OpenAirWriter();
            using (var outfile = new StreamWriter(source +".OpenAir.txt"))
                outfile.Write(openAirWriter.WriteToOpenAir(airspaceData));
            report("Generated OpenAir.");

            //Parse from OpenAir
            airspaceData = ParseFile(new OpenAirParser(), source + ".OpenAir.txt");

            //Re-Generate Kml
            kmlWriter.WriteToKml(airspaceData).Save(source + ".reparsed.kml");

            //Re-Generate OpenAir
            using (var outfile = new StreamWriter(source + ".reparsed.OpenAir.txt"))
                outfile.Write(openAirWriter.WriteToOpenAir(airspaceData));
        }

        private Airspaces ParseFile(Parser parser, string file)
        {
            using (var inFile = new StreamReader(file))
            {
                resetProgress();
                report("Parsing " + parser.FormatName + " ...");
                parser.DeclareTaskSize += new DeclareTaskSizeEventHandler(parser_DeterminedTotalTaskSize);
                parser.TaskProgress += new EventHandler(parser_ParseProgress);
                parser.Warning += new Parser.ConvertEventHandler(parser_ParseWarning);
                var result = parser.Parse("Australian Airspace", inFile.ReadToEnd());
                report("Parsed " + parser.FormatName + ". Airspaces found" + (parser.cancel ? " before failing" : "") + ": " + result.GetAirspacesCount());
                return result;
            }
        }

        void parser_DeterminedTotalTaskSize(object sender, DeclareTaskSizeEventArgs e)
        {
            progress.InvokeIfReq(f => f.Maximum = e.Size);
        }

        void parser_ParseProgress(object sender, EventArgs e)
        {
            progress.InvokeIfReq(f => f.PerformStep());
        }

        void resetProgress()
        {
            progress.InvokeIfReq(f => f.Value = 0);
        }

        void parser_ParseWarning(object sender, Parser.ConvertEventArgs e)
        {
            report((e.Fatal ? "[Fatal, aborting:] " : "") + e.Description + "\n" + e.Source);
        }

        private void reportNewLine()
        {
            info.InvokeIfReq(f => f.AppendText(Environment.NewLine));
        }

        private void report(string text)
        {
            info.InvokeIfReq(f => f.AppendText(DateTime.Now.ToLongTimeString() + "." + DateTime.Now.ToString("fff") + ": " + text.Replace("\n", Environment.NewLine) + Environment.NewLine));
        }

        private void browseSource_Click(object sender, EventArgs e)
        {
            using (var fileBrowser = new OpenFileDialog())
                browseForFile(sourceTextBox, "Source File", fileBrowser);
        }

        private void browseTarget_Click(object sender, EventArgs e)
        {
            using (var fileBrowser = new SaveFileDialog())
                browseForFile(targetTextBox, "Target File", fileBrowser);
        }

        private void browseForFile(TextBox textBox, string description, FileDialog dialog)
        {
            try
            {
                dialog.InitialDirectory = textBox.Text;
                dialog.Title = description;
                dialog.Filter = "Airspace Data|*.txt;*.suo|All Files|*.*";

                if (dialog.ShowDialog() == DialogResult.OK)
                    textBox.Text = dialog.FileName;
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }

    public static class ISynchronizeInvokeExtensions
    {
        public static void InvokeIfReq<T>(this T @this, Action<T> action) where T : ISynchronizeInvoke
        {
            if (@this.InvokeRequired)
                @this.Invoke(action, new object[] { @this });
            else
                action(@this);
        }
    }
}

