﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geo;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml;
using Kml;
using System.Drawing;

namespace Airspace
{
    class KmlWriter : Converter
    {
        override public string FormatName
        { get { return "KML"; } }

        // ToDo: Add airspace 'shadow', a ground level outline to help identify lower lewel altitude?
        private const double maxArcSegmentDeg = 15;//5;
        private const double maxLineSegmentLength = 3 * 1e5; // A line longer than this will be broken up into shorter segments to avoid dropping under the surface of earth
        private const int CoordDecimals = 5; //Number of fractional digits for coordinates
        private const int AltDecimals = 0; //Number of fractional digits for altitude
        private const double MeterPerFoot = .3048;

        public KmlDocument WriteToKml(Airspaces airspaceData)
        {
            var kmlDoc = new KmlDocument(true);
            kmlDoc.KmlDocumentElement.AppendKmlElement("name", airspaceData.Name);
            if (airspaceData.ValidDate == DateTime.MinValue)
                kmlDoc.KmlDocumentElement.AppendKmlElement("description", "Inofficial.");
            else
                kmlDoc.KmlDocumentElement.AppendKmlElement("description", "Inofficial. Valid " + airspaceData.ValidDate.ToString("yyyy MMMM d"));
            var sectionsDisplayOptions = GetSectionsDisplayOptions();

            AddStyles(kmlDoc.KmlDocumentElement);

            RaiseDeclareTaskSize(airspaceData.Sections.Count);
            foreach (Section section in airspaceData.Sections)
            {
                SectionDisplayOptions sectionDisplayOptions = sectionsDisplayOptions[section.Name.ToUpper()];
                if (!sectionDisplayOptions.Discard)
                {
                    var folderElement = kmlDoc.KmlDocumentElement.AppendKmlElement("Folder");
                    folderElement.AppendKmlElement("name", sectionDisplayOptions.Name);
                    folderElement.AppendKmlElement("open", sectionDisplayOptions.Open ? "1" : "0");

                    foreach (var airspace in section.Airspaces)
                        AddAirspace(folderElement, airspace, sectionDisplayOptions);
                }
                RaiseTaskProgress();
            }

            return kmlDoc;
        }

        private static Dictionary<string, SectionDisplayOptions> GetSectionsDisplayOptions()
        {
            var sectionsDisplayOptions = new Dictionary<string, SectionDisplayOptions>();
            sectionsDisplayOptions.Add("FLIGHT INFORMATION REGIONS", new SectionDisplayOptions() { Name = "Flight Information Regions", Style = "FIR" });
            sectionsDisplayOptions.Add("CTR", new SectionDisplayOptions() { Name = "CTR", Visibility = true, Style = "CTR" });
            sectionsDisplayOptions.Add("CONTROL ZONES C", new SectionDisplayOptions() { Name = "Control Zones C", Visibility = true, Style = "CTR" });
            sectionsDisplayOptions.Add("CONTROL ZONES D", new SectionDisplayOptions() { Name = "Control Zones D", Visibility = true, Style = "CTR" });
            sectionsDisplayOptions.Add("A", new SectionDisplayOptions() { Name = "Controlled Airspace A", Style = "A" });
            sectionsDisplayOptions.Add("CONTROLLED AIRSPACE A", new SectionDisplayOptions() { Name = "Controlled Airspace A", Style = "A" });
            sectionsDisplayOptions.Add("CONTROLLED AIRSPACE B", new SectionDisplayOptions() { Name = "Controlled Airspace B", Visibility = true, Style = "BCD" });
            sectionsDisplayOptions.Add("C", new SectionDisplayOptions() { Name = "Controlled Airspace C", Visibility = true, Style = "BCD" });
            sectionsDisplayOptions.Add("CONTROLLED AIRSPACE C", new SectionDisplayOptions() { Name = "Controlled Airspace C", Visibility = true, Style = "BCD" });
            sectionsDisplayOptions.Add("D", new SectionDisplayOptions() { Name = "Controlled Airspace D", Visibility = true, Style = "BCD" });
            sectionsDisplayOptions.Add("CONTROLLED AIRSPACE D", new SectionDisplayOptions() { Name = "Controlled Airspace D", Visibility = true, Style = "BCD" });
            sectionsDisplayOptions.Add("E", new SectionDisplayOptions() { Name = "Controlled Airspace E", Visibility = true, Style = "E" });
            sectionsDisplayOptions.Add("CONTROLLED AIRSPACE E", new SectionDisplayOptions() { Name = "Controlled Airspace E", Visibility = true, Style = "E" });
            sectionsDisplayOptions.Add("CONTROLLED AIRSPACE F", new SectionDisplayOptions() { Name = "Controlled Airspace F", Visibility = true, Style = "F" });
            sectionsDisplayOptions.Add("OCEANIC CONTROL AREAS", new SectionDisplayOptions() { Name = "Oceanic Control Areas", Style = "Oceanic" });
            sectionsDisplayOptions.Add("ATC SECTORS LOW", new SectionDisplayOptions() { Name = "ATC Sectors Low", Style = "ATCSectorLow" });
            sectionsDisplayOptions.Add("ATC SECTORS HIGH/LOW", new SectionDisplayOptions() { Name = "ATC Sectors High/Low", Style = "ATCSectorHighLow" });
            sectionsDisplayOptions.Add("E AIRSPACE FREQUENCY BOUNDARIES", new SectionDisplayOptions() { Name = "E Airspace Frequency Boundaries", Style = "EFreq" });
            sectionsDisplayOptions.Add("FLIGHT INFORMATION AREAS", new SectionDisplayOptions() { Name = "Flight Information Areas", Style = "FIA" });
            sectionsDisplayOptions.Add("R", new SectionDisplayOptions() { Name = "Restricted Area", Visibility = true, Style = "Restricted" });
            sectionsDisplayOptions.Add("RESTRICTED AREA", new SectionDisplayOptions() { Name = "Restricted Area", Visibility = true, Style = "Restricted" });
            sectionsDisplayOptions.Add("PROHIBITED AREA", new SectionDisplayOptions() { Name = "Prohibited Area", Visibility = true, Style = "Prohibited" });
            sectionsDisplayOptions.Add("DANGER AREA", new SectionDisplayOptions() { Name = "Danger Area", Visibility = true, Style = "Danger" });
            sectionsDisplayOptions.Add("GP", new SectionDisplayOptions() { Name = "Glider Prohibited", Visibility = true, Style = "Danger" });
            sectionsDisplayOptions.Add("Q", new SectionDisplayOptions() { Name = "Q [?]", Visibility = true, Style = "yellow" });
            sectionsDisplayOptions.Add("G", new SectionDisplayOptions() { Name = "Uncontrolled Airspace G", Visibility = true, Style = "green" });
            sectionsDisplayOptions.Add("LA", new SectionDisplayOptions() { Name = "Landing", Visibility = true, Style = "green" });
            sectionsDisplayOptions.Add("RMZ", new SectionDisplayOptions() { Name = "Radio Mandatory Zone", Visibility = true, Style = "orange" });

            return sectionsDisplayOptions;
        }

        private static void AddStyles(XmlElement parent)
        {
            AddStyleMap(parent, "FIR", "green");

            AddStyleMap(parent, "CTR", "CTR_BCD_ICAO");
            AddStyleMap(parent, "A", "A_ICAO");
            AddStyleMap(parent, "BCD", "BCD_ICAO");
            AddStyleMap(parent, "E", "green");
            AddStyleMap(parent, "F", "green");

            AddStyleMap(parent, "Restricted", "PRD_ICAO");
            AddStyleMap(parent, "Prohibited", "PRD_ICAO");
            AddStyleMap(parent, "Danger", "PRD_ICAO");

            AddStyleMap(parent, "Oceanic", "yellow");
            AddStyleMap(parent, "ATCSectorLow", "yellow");
            AddStyleMap(parent, "ATCSectorHighLow", "yellow");
            AddStyleMap(parent, "EFreq", "yellow");
            AddStyleMap(parent, "FIA", "yellow");


            AddPolyStyle(parent, "highlight", Color.Magenta, "bf");

            AddPolyStyle(parent, "red_Normal", Color.Red, "3f");
            AddPolyStyle(parent, "A_Normal", "ff595f74", "3fbac2d5");
            AddPolyStyle(parent, "BCD_Normal", "ff595f74", "3fbac2d5");
            AddPolyStyle(parent, "E_Normal", "ff5b627f", "3f5b627f", 1.5);
            AddPolyStyle(parent, "F_Normal", "ff5b627f", "3f5b627f");

            AddPolyStyle(parent, "A_ICAO", Color.Red, "3f");
            AddPolyStyle(parent, "BCD_ICAO", Color.Blue, "3f");
            AddPolyStyle(parent, "EF_ICAO", "ffff0000");
            AddPolyStyle(parent, "CTR_A_ICAO", "ffff0000", "3f0000ff");
            AddPolyStyle(parent, "CTR_BCD_ICAO", "ffff0000", "3f0000ff");
            AddPolyStyle(parent, "FIR_ICAO", "ffff0000");
            AddPolyStyle(parent, "PRD_ICAO", Color.Red, "3f");

            AddPolyStyle(parent, "green", Color.Green, "3f");
            AddPolyStyle(parent, "yellow", Color.Yellow, "3f");
            AddPolyStyle(parent, "orange", Color.Orange, "3f");
        }

        private static void AddStyleMap(XmlElement parent, string name, string normal, string highlight = "highlight")
        {
            var style = parent.AppendKmlElement("StyleMap", null, name);

            XmlElement styleElement;

            styleElement = style.AppendKmlElement("Pair");
            styleElement.AppendKmlElement("key", "normal");
            styleElement.AppendKmlElement("styleUrl", "#" + normal);

            styleElement = style.AppendKmlElement("Pair");
            styleElement.AppendKmlElement("key", "highlight");
            styleElement.AppendKmlElement("styleUrl", "#" + highlight);
        }

        private static void AddPolyStyle(XmlElement parent, string name, Color lineColor, string polyColorAlpha, double? lineWidth = null, Boolean labelVisibility = true)
        {
            AddPolyStyle(parent, name, ColorToABGR(lineColor), ColorToABGR(lineColor, polyColorAlpha), lineWidth, labelVisibility);
        }

        private static string ColorToABGR(Color color, string forceAlpha = null)
        {
            return (forceAlpha ?? color.A.ToString("X2")) + color.B.ToString("X2") + color.G.ToString("X2") + color.R.ToString("X2");
        }
        private static void AddPolyStyle(XmlElement parent, string name, string lineColourABGR, string polyColour = "", double? lineWidth = null, Boolean labelVisibility = true)
        {
            var style = parent.AppendKmlElement("Style", null, name);
            var styleElement = style.AppendKmlElement("LineStyle");
            styleElement.AppendKmlElement("color", lineColourABGR);
            if (lineWidth != null)
                styleElement.AppendKmlElement("width", lineWidth.ToString());
            if (labelVisibility)
                styleElement.AppendKmlElement("gx:labelVisibility", "1");
            styleElement = style.AppendKmlElement("PolyStyle");
            if (polyColour.Length == 0)
                styleElement.AppendKmlElement("fill", "0");
            else
                styleElement.AppendKmlElement("color", polyColour);
        }

        private void AddAirspace(XmlElement parent, Airspace airspace, SectionDisplayOptions sectionDisplayOptions)
        {
            //airspace.LowerLimit = airspace.LowerLimit ?? "0 AMSL";
            //airspace.UpperLimit = airspace.UpperLimit ?? "FL1000";
            VerticalLimitsKml vertLimits = GetVerticalLimits(airspace);
            Boolean extrude = vertLimits.LowerLimitAltMode == KmlAltitudeMode.clampToGround
                || (vertLimits.LowerLimitAltMode == KmlAltitudeMode.relativeToGround && vertLimits.LowerLimit <= 0);

            Polygon lateralLimitsPoints = GetLateralLimits(airspace.LateralLimitElements);
            if (lateralLimitsPoints.Count < 3)
                RaiseWarning("Airspace has insufficient points", airspace.Name);

            var placemark = parent.AppendKmlElement("Placemark");
            placemark.AppendKmlElement("name", airspace.Name + " " + airspace.LowerLimit + " - " + airspace.UpperLimit);
            if (!sectionDisplayOptions.Visibility)
                placemark.AppendKmlElement("visibility", "0");
            AddAirspaceDescription(placemark, airspace);
            placemark.AppendKmlElement("styleUrl", "#" + sectionDisplayOptions.Style);
            var multiGeometry = placemark.AppendKmlElement("MultiGeometry");
            Boolean? isExtrudeable;
            AddTop(multiGeometry, lateralLimitsPoints, vertLimits.UpperLimitAltMode, vertLimits.UpperLimit, extrude, out isExtrudeable);
            // Display bottom lid: (TODO: Not sure about this one.)
            //            AddTop(multiGeometry, lateralLimitsPoints, vertLimits.LowerLimitAltMode, vertLimits.LowerLimit, extrude);
            //kml.AppendLine(GetLabelKml(lateralLimitsPoints[0], vertLimits.UpperLimitAltMode, vertLimits.UpperLimit)); //TODO: Pretty useless in current form
            AddLabelLine(multiGeometry, lateralLimitsPoints, vertLimits);

            //if (!(extrude && (Boolean)isExtrudeable))
            //    AddSideWall(multiGeometry, lateralLimitsPoints, vertLimits, sectionDisplayOptions);
        }

        private static void AddLabelLine(XmlElement parent, Polygon lateralLimits, VerticalLimitsKml vertLimits)
        {
            var lineString = parent.AppendKmlElement("LineString");
            // Cannot use <gx:altitudeOffset> here due to what appears to be a GE bug - 
            // labels appear to float far above line if combining <gx:altitudeOffset> and <gx:labelVisibility>.
            lineString.AppendKmlElement("altitudeMode", vertLimits.UpperLimitAltMode.ToString());
            lineString.AppendKmlElement("coordinates", GetCoordsString(lateralLimits, vertLimits.UpperLimit));
        }

        //private static void AddLabel(XmlElement parent, LatLng point, KmlAltitudeMode altitudeMode, double altitude)
        //{
        //    var element = parent.AppendKmlElement("Point");
        //    element.AppendKmlElement("altitudeMode", altitudeMode.ToString());
        //    element.AppendKmlElement("coordinates", CoordString(point, altitude));
        //}

        private static void AddAirspaceDescription(XmlElement parent, Airspace airspace)
        {
            var description = new StringBuilder();
            AppendDescriptionField(description, "FIR", airspace.FIR);
            AppendDescriptionField(description, "Type", airspace.Type);
            if (airspace.IsJointDefenseFacility)
                AppendDescriptionField(description, "Type");
            AppendDescriptionField(description, "Conditional Status", airspace.ConditionalStatus);
            AppendDescriptionField(description, "Hazard", airspace.Hazard);
            AppendDescriptionField(description, "Notes", airspace.Notes);
            AppendDescriptionField(description, "Vertical Limits", airspace.LowerLimit + " - " + airspace.UpperLimit);
            AppendDescriptionField(description, "Hours of Activity", airspace.HoursOfActivity);
            AppendDescriptionField(description, "Controlling Authority", airspace.ControllingAuthority);
            AppendDescriptionField(description, "Contact", airspace.Contact);
            //            AppendDescriptionField(description, "Lateral Limits", airspace.LateralLimitsDesc);

            parent.AppendKmlElement("description").AppendChild(parent.OwnerDocument.CreateCDataSection(description.ToString()));
        }

        private static void AppendDescriptionField(StringBuilder description, string name)
        {
            description.AppendLine("<b>" + name + "</b><br/>");
        }

        private static void AppendDescriptionField(StringBuilder description, string name, string value)
        {
            if (!string.IsNullOrEmpty(value))
                description.AppendLine("<b>" + name + ":</b> " + value + "<br/>");
        }

        private Polygon GetLateralLimits(List<ILateralLimitElement> elements)
        {
            var poly = new Polygon();

            foreach (var element in elements)
            {
                if (element is LateralLimitElementPoint)
                {
                    LatLng dest = ((LateralLimitElementPoint)element).Point;

                    // Break very large lines up into segments
                    if (poly.Count == 0)
                        poly.Add(dest);
                    else
                    {
                        LatLng origin = poly[poly.Count - 1];
                        poly.AddRange(GetLinePoints(origin, dest));
                        poly.Add(dest);
                    }
                }
                else if (element is LateralLimitElementArc)
                {
                    var arc = (LateralLimitElementArc)element;
                    double bearingChange = LatLng.GetBearingChangeByDirection(arc.BearingStart, arc.BearingEnd, arc.Clockwise);
                    List<LatLng> arcPoints = arc.Centre.Arc(arc.Radius, arc.BearingStart, bearingChange, LatLng.DegToRad(maxArcSegmentDeg));

                    //double distStart = start.DistanceTo(arcPoints[0]);
                    //double distEnd = end.DistanceTo(arcPoints[arcPoints.Count - 1]);
                    //Debug.Assert(double.IsNaN(distStart) || distStart / arc.Radius < .025, "Arc does not match start point. Distance: " + distStart + ", radius: " + arc.Radius + ", ratio: " + distStart / radius);
                    //Debug.Assert(double.IsNaN(distEnd) || distEnd / arc.Radius < .025, "Arc does not match end point. Distance: " + distEnd + ", radius: " + arc.Radius + ", ratio: " + distEnd / radius);

                    //                    points.AddRange(arcPoints);
                    poly.AddRange(arcPoints.GetRange(1, arcPoints.Count - 2));
                }
                else if (element is LateralLimitElementCircle)
                {
                    var circle = (LateralLimitElementCircle)element;

                    poly.AddRange(circle.Centre.Circle(circle.Radius, (int)Math.Ceiling(360 / maxArcSegmentDeg)));
                }
                else if (element is LateralLimitElementAlongFeature)
                {
                    //TODO: Could display these separately, labeled
                    //Console.WriteLine("Ignoring feature line: " + source.Groups["feature"].Value);
                }
                else
                    RaiseError("Element not recognised", element.ToString());
            }

            // Connect last point to first (but not duplicating start and end point)
            //if (poly[0].lat == poly[poly.Count - 1].lat && poly[0].lng == poly[poly.Count - 1].lng)
            //    poly.RemoveAt(poly.Count - 1);
            poly.AddRange(GetLinePoints(poly[poly.Count - 1], poly[0]));

            if (poly.GetHandedness() == -1)
                poly.Reverse();

            return poly;
        }

        private static List<LatLng> GetLinePoints(LatLng origin, LatLng dest)
        // Does not include start or end
        {
            var linePoints = new List<LatLng>();

            double distance = origin.DistanceTo(dest);
            int segments = (int)Math.Ceiling(distance / maxLineSegmentLength);
            if (Math.Abs(origin.lat) < 90) // If origin is at a pole, we can't use bearing from there - use calculate from dest instead.
            {
                double bearing = origin.BearingTo(dest);
                for (int n = 1; n < segments; n++)
                    linePoints.Add(origin.GoTo(bearing, n * distance / segments));
            }
            else
            {
                double bearing = dest.BearingTo(origin);
                for (int n = segments - 1; n > 0; n--)
                    linePoints.Add(dest.GoTo(bearing, n * distance / segments));
            }
            return linePoints;
        }

        private static void AddTop(XmlElement parent, Polygon lateralLimits, KmlAltitudeMode altitudeMode, double altitude, Boolean extrude, out Boolean? isExtrudeable)
        {
            // Solid:
            AddPolygon(parent, lateralLimits, altitudeMode, altitude, extrude, out isExtrudeable); // Always extrudeable

            //or

            //Border only:
            //AddPolygon(parent, lateralLimits, altitudeMode, altitude, 1000);
            //AddPolygon(parent, lateralLimits, altitudeMode, altitude, extrude, out isExtrudeable, 3000);
        }

        private static void AddPolygon(XmlElement parent, Polygon lateralLimits, KmlAltitudeMode altitudeMode, double altitude, double? width = null)
        {
            Boolean? isExtrudeable; // dummy variable, discarded
            AddPolygon(parent, lateralLimits, altitudeMode, altitude, false, out isExtrudeable, width);
        }
        private static void AddPolygon(XmlElement parent, Polygon lateralLimits, KmlAltitudeMode altitudeMode, double altitude, Boolean extrude, out Boolean? isExtrudeable, double? width = null)
        {
            isExtrudeable = false;
            List<Polygon> innerPolys = null;

            Polygon adjustedPoly = new Polygon(lateralLimits.Count); // create a shallow copy of the poly in case we need to close it - don't want to touch original
            adjustedPoly.AddRange(lateralLimits);

            if (width != null)
            {
                innerPolys = lateralLimits.Shrink((double)width, CoordDecimals);
                isExtrudeable = innerPolys.Count == 0;
            }

            var polygon = parent.AppendKmlElement("Polygon");
            if (extrude && (Boolean)isExtrudeable)
            {
                polygon.AppendKmlElement("extrude", "1");
                // Extruded polygons must be closed, that is start point must be equal end point, to not leave a gap in the sides. Manally add the extra point.
                adjustedPoly.Add(adjustedPoly[0]);
            }
            polygon.AppendKmlElement("altitudeMode", altitudeMode.ToString());
            var linearRing = polygon.AppendKmlElement("outerBoundaryIs").AppendKmlElement("LinearRing");
            //Sadly, Google Earth doesn't implement gx:altitudeOffset correctly (e.g. when elevation exageration is on), so not using this at the moment.
            //linearRing.AppendKmlElement("gx:altitudeOffset", altitude.ToString());
            //linearRing.AppendKmlElement("coordinates", GetCoordsString(adjustedPoints));
            linearRing.AppendKmlElement("coordinates", GetCoordsString(adjustedPoly, altitude));

            if (width != null)
                foreach (var innerPoly in innerPolys)
                {
                    linearRing = polygon.AppendKmlElement("innerBoundaryIs").AppendKmlElement("LinearRing");
                    //linearRing.AppendKmlElement("gx:altitudeOffset", altitude.ToString());
                    //linearRing.AppendKmlElement("coordinates", GetCoordsString(innerPoly));
                    linearRing.AppendKmlElement("coordinates", GetCoordsString(innerPoly, altitude));
                }
        }

        private static string GetCoordsString(List<LatLng> points, double alt = 0)
        {
            StringBuilder result = new StringBuilder();
            foreach (LatLng point in points)
                result.Append(CoordString(point, alt) + " ");
            return result.ToString();
        }

        private static string CoordString(LatLng point, double altitude = 0)
        {
            return Math.Round(point.lng, CoordDecimals) + "," + Math.Round(point.lat, CoordDecimals) + (altitude != 0 ? "," + Math.Round(altitude, AltDecimals) : "");
        }

        private static VerticalLimitsKml GetVerticalLimits(Airspace airspace)
        {
            VerticalLimitsKml result = new VerticalLimitsKml();

            long lowerLimitFeet;
            if (long.TryParse(airspace.LowerLimit, out lowerLimitFeet))
            {
                result.LowerLimit = lowerLimitFeet * MeterPerFoot;
                result.LowerLimitAltMode = KmlAltitudeMode.absolute;
            }
            else if (Regex.Match(airspace.LowerLimit, "NOTAM|SFC|GND|Base of CTA|CTA").Success)
                result.LowerLimitAltMode = KmlAltitudeMode.clampToGround;
            else
            {
                GroupCollection groups = Regex.Match(airspace.LowerLimit, @"^(?<AMSL>\d+) ?(AMSL|FT)|^(?<AGL>\d+) AGL$|^FL(?<FL>\d+)$").Groups;
                if (groups["AMSL"].Success)
                {
                    result.LowerLimit = Convert.ToInt64(groups["AMSL"].Value) * MeterPerFoot;
                    result.LowerLimitAltMode = KmlAltitudeMode.absolute;
                }
                else if (groups["AGL"].Success)
                {
                    result.LowerLimit = Convert.ToDouble(groups["AGL"].Value) * MeterPerFoot;
                    result.LowerLimitAltMode = KmlAltitudeMode.relativeToGround;
                }
                else
                {
                    result.LowerLimit = Convert.ToDouble(groups["FL"].Value) * 100 * MeterPerFoot; //1FL = 100ft
                    result.LowerLimitAltMode = KmlAltitudeMode.absolute;
                }
            }

            long upperLimitFeet;
            if (long.TryParse(airspace.UpperLimit, out upperLimitFeet))
            {
                result.UpperLimit = upperLimitFeet * MeterPerFoot;
                result.UpperLimitAltMode = KmlAltitudeMode.absolute;
            }
            else if (Regex.Match(airspace.UpperLimit, "NOTAM|UNL|Base of CTA|CTA").Success)
            {
                result.UpperLimit = result.LowerLimit;
                result.UpperLimitAltMode = result.LowerLimitAltMode;
                if (airspace.UpperLimit == "UNL")
                {
                    result.UpperLimit = 80000; //TODO: Arbitrary large number for UNL, is this what we want?
                    result.UpperLimitAltMode = KmlAltitudeMode.absolute;
                }
            }
            else
            {
                GroupCollection groups = Regex.Match(airspace.UpperLimit, @"^(?<h>\d+) ?(?<ref>AMSL|FT|ft AGL)|^FL(?<FL>\d+)$").Groups;
                if (groups["h"].Success)
                    result.UpperLimit = Convert.ToInt64(groups["h"].Value) * MeterPerFoot;
                else
                    result.UpperLimit = Convert.ToInt64(groups["FL"].Value) * 100 * MeterPerFoot; //1FL = 100ft

                result.UpperLimitAltMode = groups["ref"].Value == "FT AGL" ? KmlAltitudeMode.relativeToGround : KmlAltitudeMode.absolute;
            }
            return result;
        }

        private static void AddSideWall(XmlElement parent, Polygon lateralLimits, VerticalLimitsKml vertLimits, SectionDisplayOptions sectionDisplayOptions)
        {
            for (int n = 0; n < lateralLimits.Count; n++)
            {
                LatLng from = lateralLimits[n];
                LatLng to = lateralLimits[(n + 1) % lateralLimits.Count];
                var polyElement = parent.AppendKmlElement("Polygon");
                polyElement.AppendKmlElement("altitudeMode", "absolute");
                polyElement.AppendKmlElement("outerBoundaryIs").AppendKmlElement("LinearRing").AppendKmlElement("coordinates",
                    CoordString(from, vertLimits.UpperLimit) + " " +
                        CoordString(to, vertLimits.UpperLimit) + " " +
                            CoordString(to, vertLimits.LowerLimit) + " " +
                                CoordString(from, vertLimits.LowerLimit) + " ");
            }
        }
    }

    public struct SectionDisplayOptions
    {
        public string Name;
        public Boolean Discard;
        public Boolean Visibility;
        public Boolean Open;
        public string Style;
    }

    public struct VerticalLimitsKml
    {
        public double LowerLimit;
        public KmlAltitudeMode LowerLimitAltMode;
        public double UpperLimit;
        public KmlAltitudeMode UpperLimitAltMode;
    }

    public enum KmlAltitudeMode
    {
        clampToGround,
        relativeToGround,
        absolute
    }
}
