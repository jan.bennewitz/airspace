﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geo;

namespace Airspace
{
    public struct Airspaces
    {
        public string Name;
        public DateTime ValidDate;
        public List<Section> Sections;

        public long GetAirspacesCount()
        {
            long i = 0;
            foreach (var section in Sections)
            {
                i += section.Airspaces.Count;
            }
            return i;
        }

        internal void RemoveRepeatedLateralLimitsElements()
        {
            foreach (var section in Sections)
                foreach (var airspace in section.Airspaces)
                {
                    int i = 0;
                    while (i < airspace.LateralLimitElements.Count && airspace.LateralLimitElements.Count > 1)
                    {
                        ILateralLimitElement preceding;
                        if (i == 0)
                            preceding = airspace.LateralLimitElements[airspace.LateralLimitElements.Count - 1];
                        else
                            preceding = airspace.LateralLimitElements[i - 1];
                        if (airspace.LateralLimitElements[i].ToString() == preceding.ToString())
                            airspace.LateralLimitElements.RemoveAt(i);
                        else
                            i++;
                    }
                }
        }
    }

    public struct Section
    {
        public string Name;
        public List<Airspace> Airspaces;

        public Section(string name)
        {
            Name = name;
            Airspaces = new List<Airspace>();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public struct Airspace
    {
        public string FIR;
        public string Name;
        public string Type;
        public Boolean IsJointDefenseFacility;
        public string ConditionalStatus;
        public string Hazard;
        public string LateralLimitsDesc;
        public List<ILateralLimitElement> LateralLimitElements;
        public string Notes;
        public string LowerLimit;
        public string UpperLimit;
        public string HoursOfActivity;
        public string ControllingAuthority;
        public string Contact;
        public string Frequency;

        public override string ToString()
        {
            return Name;
        }
    }

    public interface ILateralLimitElement
    { }

    public struct LateralLimitElementPoint : ILateralLimitElement
    {
        public LatLng Point;

        public override string ToString()
        {
            return "Point " + Point.ToString();
        }
    }

    public struct LateralLimitElementCircle : ILateralLimitElement
    {
        public LatLng Centre;
        public Double Radius;

        public LateralLimitElementCircle(LatLng centre, double radius)
        {
            Centre = centre;
            Radius = radius;
        }

        public override string ToString()
        {
            return "Circle of radius " + Radius + " around point " + Centre.ToString();
        }
    }

    public struct LateralLimitElementArc : ILateralLimitElement
    {
        public LatLng Centre;
        public Double Radius;
        public Double BearingStart;
        public Double BearingEnd;
        public Boolean Clockwise;

        public override string ToString()
        {
            return (Clockwise ? "Clockwise" : "Anticlockwise") + " arc of radius " + Radius + " around point " + Centre.ToString() + " from " + BearingStart + " to " + BearingEnd;
        }
    }

    public struct LateralLimitElementAlongFeature : ILateralLimitElement
    {
        public String FeatureDescription;

        public override string ToString()
        {
            return FeatureDescription;
        }
    }
}
