﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Geo;

namespace Airspace
{
    class OpenAirParser : Parser
    {
        public override string FormatName { get { return "OpenAir"; } }

        public override Airspaces Parse(string name, string source)
        {
            var airspaces = new Airspaces()
            {
                Name = name,
                Sections = new List<Section>()
            };

            var sectionsIndex = new Dictionary<string, Section>();

            LatLng? variableCoordinate = null;

            var airspacesSource = Regex.Split(source, @"(?=^AC .+$)", RegexOptions.Multiline);
            RaiseDeclareTaskSize(airspacesSource.Length);
            foreach (string airspaceSource in airspacesSource)
            {
                String description;
                Airspace airspace = ParseAirspace(airspaceSource, ref variableCoordinate, out description);

                if (!(airspace.Name == null))
                {
                    ParseExtendedFields(ref airspace, description);

                    // Sort into sections
                    if (!sectionsIndex.ContainsKey(airspace.Type))
                    {
                        Section newSection = new Section(airspace.Type);
                        airspaces.Sections.Add(newSection);
                        sectionsIndex.Add(airspace.Type, newSection);
                    }
                    sectionsIndex[airspace.Type].Airspaces.Add(airspace);
                }
                RaiseTaskProgress();
                if (cancel) break;
            }
            return airspaces;
        }

        private Airspace ParseAirspace(string airspaceSource, ref LatLng? variableCoordinate, out String description)
        {
            Airspace airspace = new Airspace
            {
                LateralLimitElements = new List<ILateralLimitElement>()
            };

            StringBuilder descriptionBuilder = new StringBuilder();
            Boolean variableArcDirectionIsPositive = true; // Reset at start of airspace as per OpenAir spec
            foreach (string lineSource in airspaceSource.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (!lineSource.StartsWith("*")) // Ignore comments
                {
                    string[] line = lineSource.Split(new char[] { ' ' }, 2);
                    string lineId = line[0];
                    string parameters = line[1];

                    switch (lineId)
                    {
                        case "AC": SetAirspaceField(ref airspace, "Type", parameters); break;
                        case "AN": SetAirspaceField(ref airspace, "Name", parameters); break;
                        case "AH": SetAirspaceField(ref airspace, "UpperLimit", parameters); break;
                        case "AL": SetAirspaceField(ref airspace, "LowerLimit", parameters); break;
                        case "AF": SetAirspaceField(ref airspace, "Frequency", parameters); break;
                        case "AD": descriptionBuilder.AppendLine(parameters); break;
                        case "DP": airspace.LateralLimitElements.Add(new LateralLimitElementPoint() { Point = ParsePoint(parameters) }); break;
                        case "DA": airspace.LateralLimitElements.Add(ParseArcBearingsAndRadius(parameters,variableArcDirectionIsPositive,(LatLng)variableCoordinate));break;
                        case "DB": airspace.LateralLimitElements.Add(ParseArcPointToPoint(parameters, variableArcDirectionIsPositive, (LatLng)variableCoordinate)); break;
                        case "DC": airspace.LateralLimitElements.Add(ParseCircle((LatLng)variableCoordinate, parameters)); break;
                        case "DY": break;//DY coordinate ; add a segment of an airway (NYI) - Ignore
                        case "DF": airspace.LateralLimitElements.Add(new LateralLimitElementAlongFeature() { FeatureDescription = parameters }); break;
                        case "V": // set variable
                            string[] param = parameters.Split(new char[] { '=' }, 2);
                            switch (param[0])
                            {
                                case "D":
                                    switch (param[1])
                                    {
                                        case "+": variableArcDirectionIsPositive = true; break;
                                        case "-": variableArcDirectionIsPositive = false; break;
                                        default: RaiseError("Unrecongised value for parameter " + parameters, lineSource); break;
                                    }
                                    break;
                                case "X": variableCoordinate = ParsePoint(param[1]); break;
                                case "W":
                                case "Z": break; // Ignore
                                default: RaiseError("Unrecognised parameter " + parameters, lineSource); break;
                            }
                            break;
                        default: RaiseError("Unrecognised line", lineSource); break;
                    }
                }
                if (cancel) break;
            }
            description = descriptionBuilder.ToString();
            return airspace;
        }

        private static LateralLimitElementCircle ParseCircle(LatLng centre, string parameters)
        {
            //DC radius ; draw a circle (center taken from the previous V X=...  record, radius in nm
            return new LateralLimitElementCircle(centre, double.Parse(parameters) * LatLng.MetersPerNauticalMile);
        }

        private static LateralLimitElementArc ParseArcBearingsAndRadius(string parameters, Boolean variableArcDirectionIsPositive, LatLng centre)
        {
            // DA radius, angleStart, angleEnd ; add an arc, angles in degrees, radius in nm (set center using V X=...)
            string[] arcParams = parameters.Split(new char[] { ',' }, 3);
            return new LateralLimitElementArc()
            {
                Centre = centre,
                Radius = double.Parse(arcParams[0]) * LatLng.MetersPerNauticalMile,
                BearingStart = LatLng.DegToRad(double.Parse(arcParams[1])),
                BearingEnd = LatLng.DegToRad(double.Parse(arcParams[2])),
                Clockwise = variableArcDirectionIsPositive
            };
        }

        private static LateralLimitElementArc ParseArcPointToPoint(string parameters, Boolean arcDirectionIsPositive, LatLng centre)
        {
            //DB coordinate1, coordinate2         ; add an arc, from coordinate1 to coordinate2 (set center using V X=...)
            string[] points = parameters.Split(new char[] { ',' }, 2);
            LatLng fromPt = ParsePoint(points[0]);
            LatLng toPt = ParsePoint(points[1]);

            return new LateralLimitElementArc
            {
                Centre = centre,
                BearingStart = centre.BearingTo(fromPt),
                BearingEnd = centre.BearingTo(toPt),
                Radius = (centre.DistanceTo(fromPt) + centre.DistanceTo(toPt)) / 2,
                Clockwise = arcDirectionIsPositive
            };
        }

        private void ParseExtendedFields(ref Airspace airspace, String description)
        {
            foreach (var item in Regex.Split(description, "^:", RegexOptions.Multiline))
            {
                if (item.Length > 0)
                {
                    var groups = item.TrimEnd(new char[] { ' ', '\r', '\n' }).Split(new string[] { "\r\n", "\n" }, 2, StringSplitOptions.None);
                    string fieldSource = groups[0];
                    string field;
                    switch (fieldSource.ToLower())
                    {
                        case "hours of activity": field = "HoursOfActivity"; break;
                        case "controlling authority": field = "ControllingAuthority"; break;
                        case "joint defense facility": field = "IsJointDefenseFacility"; break;
                        case "conditional status": field = "ConditionalStatus"; break;

                        case "fir":
                        case "type":
                        case "contact":
                        case "notes":
                        case "hazard":
                            field = fieldSource; break;

                        default:
                            RaiseError("Unrecognised field", fieldSource);
                            field = null;
                            break;
                    }

                    ValueType vAirspace = airspace;
                    System.Reflection.FieldInfo fieldInfo = airspace.GetType().GetField(field);//, System.Reflection.BindingFlags.GetField);
                    if (fieldInfo.FieldType.FullName == "System.Boolean")
                        fieldInfo.SetValue(vAirspace, true);
                    else
                    {
                        if (!(field == "Type" || fieldInfo.GetValue(airspace) == null))
                            RaiseError("Duplicate element " + field, airspace.Name);
                        fieldInfo.SetValue(vAirspace, groups[1]);
                    }

                    airspace = (Airspace)vAirspace;
                }
            }
        }

        private static LatLng ParsePoint(string source)
        {
            var groups = Regex.Match(source, @"(?<latDeg>\d+):(?<latMin>\d+):(?<latSec>[0-9,\.]+) (?<latHemis>N|S) +?(?<lngDeg>\d+):(?<lngMin>\d+):(?<lngSec>[0-9,\.]+) (?<lngHemis>E|W)").Groups;
            return new LatLng(
                groups["latHemis"].Value == "N" ? HemisphereNS.N : HemisphereNS.S, ushort.Parse(groups["latDeg"].Value), ushort.Parse(groups["latMin"].Value), float.Parse(groups["latSec"].Value),
                groups["lngHemis"].Value == "E" ? HemisphereEW.E : HemisphereEW.W, ushort.Parse(groups["lngDeg"].Value), ushort.Parse(groups["lngMin"].Value), float.Parse(groups["lngSec"].Value)
                );
        }

        private Airspace SetAirspaceField(ref Airspace airspace, string field, string value)
        {
            System.Reflection.FieldInfo fieldInfo = airspace.GetType().GetField(field);//, System.Reflection.BindingFlags.GetField);
            if (fieldInfo.GetValue(airspace) != null)
                //RaiseError("Duplicate element " + field, airspace.Name);
                value = value + " / " + fieldInfo.GetValue(airspace);
            ValueType vAirspace = airspace;
            fieldInfo.SetValue(vAirspace, value);
            airspace = (Airspace)vAirspace;
            return airspace;
        }
    }
}
