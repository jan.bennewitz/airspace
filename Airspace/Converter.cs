﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airspace
{
    abstract class Converter
    {
        abstract public string FormatName { get; }

        public bool cancel { get; set; }

        public class ConvertEventArgs : EventArgs
        {
            public String Description { get; private set; }
            public String Source { get; private set; }
            public Boolean Fatal { get; private set; }

            public ConvertEventArgs(string description, string source, Boolean fatal)
            {
                Description = description;
                Source = source;
                Fatal = fatal;
            }
        }
        public delegate void ConvertEventHandler(object sender, ConvertEventArgs e);
        public event ConvertEventHandler Warning;
        protected void OnWarning(ConvertEventArgs e)
        {
            if (Warning != null)
                Warning(this, e);
        }

        protected void RaiseWarning(string description, string source)
        {
            OnWarning(new ConvertEventArgs(description, source, false));
        }

        protected void RaiseError(string description, string source)
        {
            OnWarning(new ConvertEventArgs(description, source, true));
            cancel = true;
        }

        protected void RaiseDeclareTaskSize(int size)
        {
            // Declare how many TaskProgress events will be raised. (Do this before raising the first.)
            if (DeclareTaskSize != null)
                DeclareTaskSize(this, new DeclareTaskSizeEventArgs(size));
        }
        public event DeclareTaskSizeEventHandler DeclareTaskSize;

        protected void RaiseTaskProgress()
        {
            if (TaskProgress != null)
                TaskProgress(this, new EventArgs());
        }
        public event EventHandler TaskProgress;
    }

    public class DeclareTaskSizeEventArgs : EventArgs
    {
        public DeclareTaskSizeEventArgs(Int32 size)
        {
            Size = size;
        }
        public Int32 Size { get; private set; }
    }
    public delegate void DeclareTaskSizeEventHandler(object sender, DeclareTaskSizeEventArgs e);
}
