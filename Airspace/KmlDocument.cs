﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

namespace Kml
{
    class KmlDocument : XmlDocument
    {
        private const string KmlNamespace = "http://www.opengis.net/kml/2.2";
        private const string KmlExtNamespace = "http://www.google.com/kml/ext/2.2";

        private XmlElement kmlDocumentElement;
        public XmlElement KmlDocumentElement
        {
            get { return kmlDocumentElement; }
        }

        public KmlDocument(Boolean includeGXNamespace = false)
        {
            AppendChild(CreateXmlDeclaration("1.0", "UTF-8", null));
            var kmlElement = CreateKmlElement("kml");
            if (includeGXNamespace)
                kmlElement.SetAttribute("xmlns:gx", KmlExtNamespace);
            AppendChild(kmlElement);
            kmlDocumentElement = CreateKmlElement("Document");
            kmlElement.AppendChild(kmlDocumentElement);
        }

        public XmlElement CreateKmlElement(String qualifiedName, String text = null, String id = null)
        {
            var element = CreateElement(qualifiedName, qualifiedName.Substring(0, 3) == "gx:" ? KmlExtNamespace : KmlNamespace);
            if (!String.IsNullOrEmpty(text))
                element.AppendChild(CreateTextNode(text));
            if (!String.IsNullOrEmpty(id))
            {
                var attribute = CreateAttribute("id");
                attribute.Value = id;
                element.Attributes.Append(attribute);
            }
            return element;
        }

        public void SaveAsKmz(string outFile)
        {
            var kmlBytes= new System.Text.UTF8Encoding().GetBytes(OuterXml);
            using (System.IO.MemoryStream ms = new MemoryStream())
            using (ZipOutputStream zs = new ZipOutputStream(ms))
            {
                zs.SetLevel(6);
                zs.IsStreamOwner = true;

                //create zipped kml entry
                ZipEntry zipEntry = new ZipEntry("Doc.kml");

                zipEntry.DateTime = DateTime.Now;
                zipEntry.Size = kmlBytes.Length;

                zs.PutNextEntry(zipEntry);
                zs.Write(kmlBytes, 0, kmlBytes.Length);
                zs.CloseEntry();

                zs.Finish();
                //write to disk
                using (FileStream kmz = new FileStream(outFile, FileMode.Create))
                {
                    kmz.Write(ms.ToArray(), 0, (int)ms.Length);
                }
            }
        }
    }

    public static class KmlElement
    {
        //Extends XmlElement
        public static XmlElement AppendKmlElement(this XmlElement element, String qualifiedName, String text = null, String id = null)
        {
            var newNode = ((KmlDocument)element.OwnerDocument).CreateKmlElement(qualifiedName, text, id);
            return (XmlElement)element.AppendChild(newNode);
        }
    }
}
