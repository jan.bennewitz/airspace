﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClipperLib;

namespace Geo
{
    public class Polygon : List<LatLng>
    {
        public List<IntPoint> ClipperPolyFromPoints(double scale)
        {
            var clipperPoly = new List<IntPoint>(Count);
            for (int i = 0; i < Count; i++)
                clipperPoly.Add(new IntPoint((long)Math.Round(this[i].lat * scale), (long)Math.Round(this[i].lng * scale)));
            return clipperPoly;
        }

        public Polygon() { }
        public Polygon(int capacity) : base(capacity) { }
        public Polygon(IEnumerable<LatLng> collection) : base(collection) { }

        public Polygon(List<IntPoint> clipperPoly, double scale)
            : base(clipperPoly.Count)
        {
            //PointsFromClipperPoly(List<IntPoint> clipperPoly)
            for (int i = 0; i < clipperPoly.Count; i++)
                Add(new LatLng(clipperPoly[i].X / scale, clipperPoly[i].Y / scale));

            // TODO: Unnecessary? Where does this go if not?
            Add(this[0]); //Close the ring
        }

        public int GetHandedness()
        {
            double area = 0;
            for (int i = 0; i < Count; i++)
            {
                var point = this[i];
                var nextPoint = this[(i + 1) % Count];
                area += (point.lat * nextPoint.lng - point.lng * nextPoint.lat) / 2;
            }
            return Math.Sign(area);
        }

        public List<Polygon> Shrink(double shrinkBy, double precision)
        {
            var shrunkPoints = new Polygon();
            for (int i = 0; i < Count; i++)
            {
                LatLng previousPt = this[(Count + i - 1) % Count];
                LatLng thisPt = this[i];
                LatLng nextPt = this[(i + 1) % Count];

                var innerAngle = (thisPt.BearingTo(previousPt) - thisPt.BearingTo(nextPt) + 2 * Math.PI) % (Math.PI * 2);
                var bearingChange = thisPt.GetBearingChange(previousPt, nextPt);

                if (bearingChange <= 0)
                    shrunkPoints.Add(thisPt.GoTo(thisPt.BearingTo(nextPt) + (innerAngle / 2), shrinkBy / Math.Cos(bearingChange / 2)));
                else
                {
                    if (Math.Abs(thisPt.lat) == 90)
                    {
                        shrunkPoints.Add(new LatLng(
                            Math.Sign(thisPt.lat) * (90 - LatLng.RadToDeg(shrinkBy / (Math.Cos(bearingChange / 2) * LatLng.EarthRadius))),
                            nextPt.lng + LatLng.RadToDeg(innerAngle) / 2
                            ));
                    
                        //shrunkPoints.Add(new LatLng(Math.Sign(thisPt.lat) * (90 - LatLng.RadToDeg(shrinkBy / LatLng.EarthRadius)), previousPt.lng - 90));
                        //shrunkPoints.Add(thisPt);
                        //shrunkPoints.Add(new LatLng(Math.Sign(nextPt.lat) * (90 - LatLng.RadToDeg(shrinkBy / LatLng.EarthRadius)), nextPt.lng + 90));

                        //System.Diagnostics.Debug.Assert(thisPt.DistanceTo(shrunkPoints[shrunkPoints.Count - 1]) - shrinkBy < 1);
                    }
                    else
                    {
                        shrunkPoints.Add(thisPt.GoTo(thisPt.BearingTo(previousPt) - Math.PI / 2, shrinkBy));
                        shrunkPoints.Add(thisPt);
                        shrunkPoints.Add(thisPt.GoTo(thisPt.BearingTo(nextPt) + Math.PI / 2, shrinkBy));
                    }
                }
            }

            Clipper clpr = new Clipper();
            double scale = Math.Pow(10, precision);
            var clipperPolys = new List<List<IntPoint>>();
            clipperPolys.Add(shrunkPoints.ClipperPolyFromPoints(scale));
            clpr.AddPolygons(clipperPolys, PolyType.ptSubject);
            IntRect r = clpr.GetBounds();
            var outer = new List<IntPoint>(4);

            outer.Add(new IntPoint(r.left - 10, r.bottom + 10));
            outer.Add(new IntPoint(r.right + 10, r.bottom + 10));
            outer.Add(new IntPoint(r.right + 10, r.top - 10));
            outer.Add(new IntPoint(r.left - 10, r.top - 10));

            clpr.AddPolygon(outer, PolyType.ptSubject);
            clpr.Execute(ClipType.ctUnion, clipperPolys, PolyFillType.pftNegative, PolyFillType.pftNegative);
            if (clipperPolys.Count > 0)
            {
                clipperPolys.RemoveAt(0);
                for (int i = 0; i < clipperPolys.Count; i++)
                    clipperPolys[i].Reverse();
            }

            var result = new List<Polygon>(clipperPolys.Count);
            foreach (var clipperPoly in clipperPolys)
                result.Add(new Polygon(clipperPoly, scale));

            return result;
        }
    }
}
