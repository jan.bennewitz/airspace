﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Geo
{
    public struct LatLng
    {
        public const double EarthRadius = 6371000; // Earth's radius in m
        public const int MetersPerNauticalMile = 1852; // (Exact)

        public double lat,
                      lng;

        public LatLng(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
        }

        /// <summary>
        /// Constructor from degrees and decimal minutes and seconds
        /// </summary>
        /// <param name="latDeg">Latitude - Degrees</param>
        /// <param name="latMin">Latitude - Minutes</param>
        /// <param name="latSec">Latitude - Seconds</param>
        /// <param name="lngDeg">Longitude - Degrees</param>
        /// <param name="lngMin">Longitude - Minutes</param>
        /// <param name="lngSec">Longitude - Seconds</param>
        public LatLng(HemisphereNS latHemisphere, UInt16 latDeg, double latMin, double latSec,
                        HemisphereEW lngHemisphere, UInt16 lngDeg, double lngMin, double lngSec)
            : this()
        {
            double absLat = latDeg + latMin / 60 + latSec / (60 * 60);
            double absLng = lngDeg + lngMin / 60 + lngSec / (60 * 60);

            if (latHemisphere == HemisphereNS.N)
                lat = absLat;
            else
                lat = -absLat;

            if (lngHemisphere == HemisphereEW.E)
                lng = absLng;
            else
                lng = -absLng;
        }

        /// <summary>
        /// Constructor from degrees and decimal minutes
        /// </summary>
        /// <param name="latDeg">Latitude - Degrees</param>
        /// <param name="latMin">Latitude - Minutes</param>
        /// <param name="lngDeg">Longitude - Degrees</param>
        /// <param name="lngMin">Longitude - Minutes</param>
        public LatLng(HemisphereNS latHemisphere, UInt16 latDeg, double latMin,
                        HemisphereEW lngHemisphere, UInt16 lngDeg, double lngMin)
            : this(latHemisphere, latDeg, latMin, 0, lngHemisphere, lngDeg, lngMin, 0) { }

        public override string ToString()
        {
            return lat + ", " + lng;
        }

        /**
         * distance(segmentPoint1, segmentPoint2) computes the distance between the this and the
         * segment [segmentPoint1, segmentPoint2]. This could probably be replaced with something that is a
         * bit more numerically stable.
         */
        // The result is in degrees
        public double DistanceFromSegment(LatLng segmentPoint1, LatLng segmentPoint2)
        {
            double u, diff = 0.0;

            if (segmentPoint1.lat == segmentPoint2.lat
                && segmentPoint1.lng == segmentPoint2.lng)
            {
                diff = Math.Sqrt(Math.Pow(segmentPoint2.lat - lat, 2)
                    + Math.Pow(segmentPoint2.lng - lng, 2));
            }
            else
            {
                u = ((lat - segmentPoint1.lat)
                    * (segmentPoint2.lat - segmentPoint1.lat) + (lng - segmentPoint1.lng)
                    * (segmentPoint2.lng - segmentPoint1.lng))
                    / (Math.Pow(segmentPoint2.lat - segmentPoint1.lat, 2)
                                  + Math.Pow(segmentPoint2.lng - segmentPoint1.lng, 2));

                if (u <= 0)
                {
                    diff = Math.Sqrt(Math.Pow(lat - segmentPoint1.lat,
                        2)
                        + Math.Pow(lng - segmentPoint1.lng, 2));
                }
                if (u >= 1)
                {
                    diff = Math.Sqrt(Math.Pow(lat - segmentPoint2.lat,
                        2)
                        + Math.Pow(lng - segmentPoint2.lng, 2));
                }
                if (0 < u && u < 1)
                {
                    diff = Math.Sqrt(Math.Pow(lat - segmentPoint1.lat
                        - u * (segmentPoint2.lat - segmentPoint1.lat), 2)
                        + Math.Pow(lng - segmentPoint1.lng - u
                            * (segmentPoint2.lng - segmentPoint1.lng), 2));
                }
            }
            return diff;
        }

        // Math adapted from http://www.movable-type.co.uk/scripts/latlong.html

        public Double DistanceTo(LatLng point)
        {
            return Math.Acos(
                    Math.Sin(DegToRad(lat)) * Math.Sin(DegToRad(point.lat)) +
                    Math.Cos(DegToRad(lat)) * Math.Cos(DegToRad(point.lat)) *
                    Math.Cos(DegToRad(point.lng) - DegToRad(lng))
                ) * EarthRadius; // simple spherical distance
        }

        //Initial bearing (forward azimuth) to target
        public double BearingTo(LatLng target) // in radians
        {
            Double y = Math.Sin(DegToRad(target.lng - lng)) * Math.Cos(DegToRad(target.lat));
            Double x = Math.Cos(DegToRad(lat)) * Math.Sin(DegToRad(target.lat)) -
                    Math.Sin(DegToRad(lat)) * Math.Cos(DegToRad(target.lat)) * Math.Cos(DegToRad(target.lng - lng));
            return Math.Atan2(y, x);
        }

        ///The along-track distance, from the start point to the closest point on the path to the end point
        public Double AlongTrackDistance(LatLng Start, LatLng End)
        {
            double distanceToStart = DistanceTo(Start);
            double bearingFromStartR = Start.BearingTo(this);
            double bearingStartToEndR = Start.BearingTo(End);
            // cross-track distance:
            double dXt = Math.Asin(Math.Sin(distanceToStart / EarthRadius) * Math.Sin(bearingFromStartR - bearingStartToEndR)) * EarthRadius;

            return Math.Acos(Math.Cos(distanceToStart / EarthRadius) / Math.Cos(dXt / EarthRadius)) * EarthRadius;
        }

        public Polygon Circle(double radius, int segments)
        {
            var circle = new Polygon();

            for (int n = 0; n < segments; n++)
            {
                double bearing = n * 2 * Math.PI / segments;
                circle.Add(GoTo(bearing, radius));
            }
            return circle;
        }

        public List<LatLng> Arc(double radius, double bearingStart, double bearingChange, double maxSegmentSizeRad)
        {
            var arcPoints = new List<LatLng>();

            int segmentsCount = (int)Math.Ceiling(Math.Abs(bearingChange) / maxSegmentSizeRad);

            for (int n = 0; n <= segmentsCount; n++)
            {
                double bearing = bearingStart + n * bearingChange / segmentsCount;
                arcPoints.Add(GoTo(bearing, radius));
            }
            return arcPoints;
        }

        public LatLng GoTo(double bearingRad, double distance)
        {
            if (Math.Abs(lat) == 90)
                throw new ArgumentException("Cannot use bearing at a pole.");

            //Formula:  lat2 = asin(sin(lat1)*cos(d/R) + cos(lat1)*sin(d/R)*cos(θ)) 
            //          lon2 = lon1 + atan2(sin(θ)*sin(d/R)*cos(lat1), cos(d/R)−sin(lat1)*sin(lat2)) 

            double angleToDest = distance / EarthRadius;
            double latR = DegToRad(lat);
            double destLatR = Math.Asin(Math.Sin(latR) * Math.Cos(angleToDest) + Math.Cos(latR) * Math.Sin(angleToDest) * Math.Cos(bearingRad));
            double destLngR = DegToRad(lng) + Math.Atan2(Math.Sin(bearingRad) * Math.Sin(angleToDest) * Math.Cos(latR), Math.Cos(angleToDest) - Math.Sin(latR) * Math.Sin(destLatR));

            return new LatLng(RadToDeg(destLatR), RadToDeg(destLngR));
        }

        public double GetBearingChange(LatLng previousPt, LatLng nextPt)
        {
            return (6 * Math.PI + BearingTo(nextPt) - BearingTo(previousPt)) % (2 * Math.PI) - Math.PI;
        }

        public static double DegToRad(double angle) { return Math.PI * angle / 180.0; }
        public static double RadToDeg(double angle) { return angle * 180.0 / Math.PI; }

        public static double GetBearingChangeByMajorMinor(double bearingStart, double bearingEnd, Boolean alongMajorArc)
        {
            double bearingChange = (bearingEnd - bearingStart + 3 * Math.PI) % (2 * Math.PI) - Math.PI;
            if (alongMajorArc)
            {
                if (bearingChange > 0)
                    bearingChange = bearingChange - 2 * Math.PI;
                else
                    bearingChange = bearingChange + 2 * Math.PI;
            }
            return bearingChange;
        }
        public static double GetBearingChangeByDirection(double bearingStart, double bearingEnd, Boolean clockwise)
        {
            if (clockwise)
                return (bearingEnd - bearingStart + 2 * Math.PI) % (2 * Math.PI);
            else
                return (bearingEnd - bearingStart - 2 * Math.PI) % (2 * Math.PI);
        }
    }

    public enum HemisphereNS
    {
        N,
        S
    }
    public enum HemisphereEW
    {
        E,
        W
    }
}
